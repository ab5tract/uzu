#!/usr/bin/env perl6

use v6;
use Uzu;

multi MAIN('config', Str :$config = 'config.yml') {
  say uzu-config(config_file => $config);
}

multi MAIN('webserver', Str :$config = 'config.yml') {
  my %config = uzu-config(config_file => $config);
  Uzu::web-server(config => %config);
}

multi MAIN('build', Str :$config = 'config.yml') {
  my %config = uzu-config(config_file => $config);
  Uzu::render(config => %config,
              no_livereload => True);
}

multi MAIN('watch', Str :$config = 'config.yml', Bool :$no-livereload) {
  my %config = uzu-config(config_file => $config);
  Uzu::watch(config => %config,
             no_livereload => ($no-livereload ?? True !! False));
}

multi MAIN('init', Str :$config = 'config.yml') {

  # config file exists, exit
  return say "Config [$config] already exists." if $config.IO ~~ :f;

  say "Uzu project initialization";
  my $project_name = prompt("Please enter project name: ");
  my $url = prompt("Please enter project url (e.g http://example.com): ");
  my $language = prompt("Please enter project language (e.g. en, ja): ");
  my $theme = prompt("Please enter project theme (e.g. default): ");

  if Uzu::init config_file  => $config,
               project_name => $project_name,
               url          => $url,
               language     => $language,
               theme        => $theme {
    say "Config [$config] successfully created.";
  }
}

multi MAIN('version') {
  say "uzu {Uzu.^ver}";
}

sub USAGE {
  say q:to/END/;
      Usage:
        uzu init          - Initialize new project
        uzu webserver     - Start local web server
        uzu build         - Render all templates to build
        uzu watch         - Start web server and re-render
                            build on template modification
        uzu version       - Print uzu version and exit

      Optional arguments:
        
        --config=         - Specify a custom config file
                            Default is `config`

        e.g. uzu --config=path/to/config.yml init 

        --no-livereload   - Disable livereload when
                            running uzu watch.
      END
}

# vim: ft=perl6
